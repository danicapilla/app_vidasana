package app.owo.com.database;

import android.content.Context;
import android.util.Log;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import app.owo.com.database.data.ContentData;
import app.owo.com.database.data.ImgData;
import app.owo.com.database.data.TextData;
import app.owo.com.enumerations.CategoryTypes;
import app.owo.com.enumerations.ImageTypes;
import app.owo.com.enumerations.LanguageTypes;
import app.owo.com.enumerations.SubCategoryTypes;
import app.owo.com.enumerations.TextTypes;

/**
 * Created by vsantoja on 1/06/15.
 */
public class NewsDao
{
    private Dao<ContentData, Integer> contentDao;
    private Dao<ImgData, Integer> imgDao;
    private Dao<TextData, Integer> textDao;
    private DatabaseHelper databaseHelper = null;
    private static HashMap<Integer, Enum> codeValueMap = new HashMap<>();

    public NewsDao(Context context) {
        try
        {
            this.databaseHelper = OpenHelperManager.getHelper(context, DatabaseHelper.class);
            this.contentDao = databaseHelper.getDao(ContentData.class);
            this.imgDao = databaseHelper.getDao(ImgData.class);
            this.textDao = databaseHelper.getDao(TextData.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public boolean saveNews(JSONArray arrayNews) {
        try
        {
            ContentData contentData;
            ImgData imgData;
            TextData textData;

            for( int i = 0 ; i < arrayNews.length() ; i++ )
            {
                codeValueMap.clear();
                for( CategoryTypes categoryTypes : CategoryTypes.values() ) {
                    codeValueMap.put(categoryTypes.getCode(), categoryTypes);
                }

                JSONObject object = arrayNews.getJSONObject(i);
                contentData = new ContentData();
                int idNew = Integer.parseInt(object.getString("id"));
                contentData.setId(idNew);
                contentData.setIdCat(CategoryTypes.get(Integer.parseInt(object.getString("idCatRef"))));

                Log.i(NewsDao.class.getName(), object.getString("idSubcat"));

                contentData.setIdSubCat(SubCategoryTypes.get(Integer.parseInt(object.getString("idSubcat"))));
                contentData.setPriority(Integer.parseInt(object.getString("priority")));

                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
                Date date = sdf.parse(object.getString("insertdate"));

                contentData.setDate(date.getTime());


                ContentData content  = contentDao.queryForId(idNew);

                if( content == null ) {
                    contentDao.create(contentData);

                    codeValueMap.clear();
                    for (ImageTypes imgType : ImageTypes.values()) {
                        codeValueMap.put(imgType.getCode(), imgType);
                    }

                    for (int j = 0; j < object.getJSONArray("imgs").length(); j++) {
                        JSONObject objectImage = object.getJSONArray("imgs").getJSONObject(j);
                        imgData = new ImgData();
                        imgData.setIdImgType((ImageTypes) codeValueMap.get(objectImage.getInt("idImageType")));
                        imgData.setIdNew(idNew);
                        imgData.setPath(objectImage.getString("path"));
                        imgDao.create(imgData);
                    }

                    codeValueMap.clear();
                    for (TextTypes textType : TextTypes.values()) {
                        codeValueMap.put(textType.getCode(), textType);
                    }

                    for (int z = 0; z < object.getJSONArray("txts").length(); z++) {
                        JSONObject objectText = object.getJSONArray("txts").getJSONObject(z);
                        textData = new TextData();
                        textData.setIdNew(idNew);
                        textData.setIdTextType((TextTypes) codeValueMap.get(objectText.getInt("idTextType")));
                        //Por ahora todos son en español
                        textData.setLanguageType(LanguageTypes.espannol);
                        textData.setText(objectText.getString("text"));
                        textDao.create(textData);
                    }
                }
            }
            databaseHelper.close();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return true;
    }

    public List<Content> getCnt (CategoryTypes idCatRef){

        Log.i(NewsDao.class.getName(), "IdCatRef: " + idCatRef);

        List<ImgData>imgs = null;
        List<TextData>txts = null;
        List<ContentData>cntsData = null;
        List<Content>cnts = new ArrayList<>();

        switch (idCatRef){

            case portada:
                try {
                    cntsData = contentDao.queryBuilder().orderBy("priority", false).orderBy("date", false).query();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                break;

            default:
                try {
                    cntsData = contentDao.queryBuilder().orderBy("priority", false).orderBy("date", false).where().eq("idCat", idCatRef).query();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                break;
        }

        if (cntsData!=null){

            for (ContentData c : cntsData){
                try {
                    imgs = imgDao.queryBuilder().where().eq("idNew", c.getId()).query();
                    txts = textDao.queryBuilder().where().eq("idNew", c.getId()).query();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                if (imgs!=null && txts!=null){
                    Content cnt = new Content(c,imgs,txts);
                    cnts.add(cnt);
                }
            }

        }
        return cnts;
    }

    public List<Content> getCntFav (){

        List<ImgData>imgs = null;
        List<TextData>txts = null;
        List<ContentData>cntsData = null;
        List<Content>cnts = new ArrayList<>();

        try
        {
            cntsData = contentDao.queryBuilder().orderBy("priority", false).orderBy("date", false).where().eq("fav", true).query();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if (cntsData!=null){

            for (ContentData c : cntsData){
                try {
                    imgs = imgDao.queryBuilder().where().eq("idNew", c.getId()).query();
                    txts = textDao.queryBuilder().where().eq("idNew", c.getId()).query();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                if (imgs!=null && txts!=null){
                    Content cnt = new Content(c,imgs,txts);
                    cnts.add(cnt);
                }
            }

        }
        return cnts;
    }

    public boolean existContent (CategoryTypes categoryTypes) {

        List<ContentData>cntsData = null;

        try {
            cntsData = contentDao.queryBuilder().where().eq("idCat", categoryTypes).query();

            if( cntsData == null || cntsData.size() == 0) return false;
            else                    return true;

        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public void changeFav (int idContent) {
        try {

            ContentData contentData = contentDao.queryForId(idContent);
            if( contentData.isFav() ) contentData.setFav(false);
            else                      contentData.setFav(true);

            contentDao.update(contentData);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void closeHelper() {
        if( databaseHelper != null ) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }
    }
}
