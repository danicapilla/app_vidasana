package app.owo.com.database.data;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

import app.owo.com.enumerations.CategoryTypes;
import app.owo.com.enumerations.SubCategoryTypes;

/**
 * Created by vsantoja on 27/05/15.
 */

@DatabaseTable
public class ContentData implements Serializable {

    @DatabaseField(id = true, useGetSet = true)
    private Integer id;

    @DatabaseField
    private long date;

    @DatabaseField
    private boolean fav;

    @DatabaseField(dataType = DataType.ENUM_INTEGER)
    private CategoryTypes idCat;

    @DatabaseField(dataType = DataType.ENUM_INTEGER)
    private SubCategoryTypes idSubCat;

    @DatabaseField
    private int priority;

    public ContentData() {
    }

    public ContentData(Integer id, long date, CategoryTypes idCat, int priority, SubCategoryTypes subCategoryTypes) {
        this.id = id;
        this.date = date;
        this.idCat = idCat;
        this.priority = priority;
        this.idSubCat = subCategoryTypes;
        this.fav = true;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public CategoryTypes getIdCat() {
        return idCat;
    }

    public void setIdCat(CategoryTypes idCat) {
        this.idCat = idCat;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public SubCategoryTypes getIdSubCat() {
        return idSubCat;
    }

    public void setIdSubCat(SubCategoryTypes idSubCat) {
        this.idSubCat = idSubCat;
    }

    public boolean isFav() {
        return fav;
    }

    public void setFav(boolean fav) {
        this.fav = fav;
    }
}
