package app.owo.com.database.data;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;

import java.io.Serializable;

import app.owo.com.enumerations.ImageTypes;

/**
 * Created by vsantoja on 28/05/15.
 */
public class ImgData implements Serializable
{
    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField(dataType = DataType.ENUM_INTEGER)
    private ImageTypes idImgType;

    @DatabaseField
    private int idNew;

    @DatabaseField
    private String path;


    public ImgData() {
    }

    public ImgData(ImageTypes idImgType, int idNew, String path) {
        this.idImgType = idImgType;
        this.idNew = idNew;
        this.path = path;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ImageTypes getIdImgType() {
        return idImgType;
    }

    public void setIdImgType(ImageTypes idImgType) {
        this.idImgType = idImgType;
    }

    public int getIdNew() {
        return idNew;
    }

    public void setIdNew(int idNew) {
        this.idNew = idNew;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
