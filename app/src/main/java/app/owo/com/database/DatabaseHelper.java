package app.owo.com.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.AndroidDatabaseConnection;
import com.j256.ormlite.android.DatabaseTableConfigUtil;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.support.DatabaseConnection;
import com.j256.ormlite.table.DatabaseTableConfig;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

import app.owo.com.Constants;
import app.owo.com.database.data.ContentData;
import app.owo.com.database.data.ImgData;
import app.owo.com.database.data.TextData;

/**
 * Created by vsantoja on 27/05/15.
 */

public class DatabaseHelper extends OrmLiteSqliteOpenHelper
{

    public DatabaseHelper (Context context) {
        super(context, Constants.DATABASE_NAME, null, Constants.DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        DatabaseConnection conn = connectionSource.getSpecialConnection();
        Log.i(DatabaseHelper.class.getName(), "OnCreate()");
        try
        {
            connectionSource.saveSpecialConnection(conn);
            TableUtils.createTable(connectionSource, ContentData.class);
            TableUtils.createTable(connectionSource, TextData.class);
            TableUtils.createTable(connectionSource, ImgData.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if (conn == null) {
            conn = new AndroidDatabaseConnection(database, true);
            try {
                connectionSource.saveSpecialConnection(conn);
            } catch (SQLException e) {
                throw new IllegalStateException("Could not save special connection", e);
            }
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        DatabaseConnection conn = connectionSource.getSpecialConnection();
        if (conn == null) {
            conn = new AndroidDatabaseConnection(database, true);
            try {
                connectionSource.saveSpecialConnection(conn);
            } catch (SQLException e) {
                throw new IllegalStateException("Could not save special connection", e);
            }
        }
        try {
            TableUtils.dropTable(connectionSource, ContentData.class, true);
            TableUtils.dropTable(connectionSource, TextData.class, true);
            TableUtils.dropTable(connectionSource, ImgData.class, true);

            TableUtils.createTable(connectionSource, ContentData.class);
            TableUtils.createTable(connectionSource, TextData.class);
            TableUtils.createTable(connectionSource, ImgData.class);

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            connectionSource.clearSpecialConnection(conn);
        }
    }

    @Override
    public void close() {
        super.close();
    }

    public  <D extends Dao<T, ?>, T> D getDao(Class<T> clazz) throws SQLException {
        // lookup the dao, possibly invoking the cached database config
        Dao<T, ?> dao = DaoManager.lookupDao(connectionSource, clazz);
        if (dao == null) {
            // try to use our new reflection magic
            DatabaseTableConfig<T> tableConfig = DatabaseTableConfigUtil.fromClass(connectionSource, clazz);
            if (tableConfig == null) {
                dao = (Dao<T, ?>) DaoManager.createDao(connectionSource, clazz);
            } else {
                dao = (Dao<T, ?>) DaoManager.createDao(connectionSource, tableConfig);
            }
        }

        @SuppressWarnings("unchecked")
        D castDao = (D) dao;
        return castDao;
    }
}
