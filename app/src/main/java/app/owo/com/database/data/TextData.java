package app.owo.com.database.data;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;

import java.io.Serializable;

import app.owo.com.enumerations.LanguageTypes;
import app.owo.com.enumerations.TextTypes;

/**
 * Created by vsantoja on 28/05/15.
 */
public class TextData implements Serializable
{
    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField(dataType = DataType.ENUM_INTEGER)
    private TextTypes idTextType;

    @DatabaseField(dataType = DataType.ENUM_INTEGER)
    private LanguageTypes languageType;

    @DatabaseField
    private int idNew;

    @DatabaseField
    private String text;


    public TextData() {
    }

    public TextData(TextTypes idTextType,LanguageTypes languageType, int idNew, String text) {
        this.idTextType = idTextType;
        this.languageType = languageType;
        this.idNew = idNew;
        this.text = text;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public TextTypes getIdTextType() {
        return idTextType;
    }

    public void setIdTextType(TextTypes idTextType) {
        this.idTextType = idTextType;
    }

    public LanguageTypes getLanguageType() {
        return languageType;
    }

    public void setLanguageType(LanguageTypes languageType) {
        this.languageType = languageType;
    }

    public int getIdNew() {
        return idNew;
    }

    public void setIdNew(int idNew) {
        this.idNew = idNew;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
