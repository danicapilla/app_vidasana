package app.owo.com.database;

import java.io.Serializable;
import java.util.List;

import app.owo.com.database.data.ContentData;
import app.owo.com.database.data.ImgData;
import app.owo.com.database.data.TextData;
import app.owo.com.enumerations.ImageTypes;
import app.owo.com.enumerations.TextTypes;

/**
 * Created by vsantoja on 9/06/15.
 */
public class Content implements Serializable{

    private ContentData cntData;
    private List<ImgData> imgs;
    private List<TextData>txts;

    public Content(){}

    public Content (ContentData cnt, List<ImgData>imgs,List<TextData> txts){

        this.cntData=cnt;
        this.imgs=imgs;
        this.txts=txts;

    }

    public ContentData getCntData() {
        return cntData;
    }

    public void setCntData(ContentData cntData) {
        this.cntData = cntData;
    }

    public List<ImgData> getImgs() {
        return imgs;
    }

    public void setImgs(List<ImgData> imgs) {
        this.imgs = imgs;
    }

    public List<TextData> getTxts() {
        return txts;
    }

    public void setTxts(List<TextData> txts) {
        this.txts = txts;
    }

    public TextData getText(TextTypes textType) {
        for(TextData text : txts) {
            if( text.getIdTextType() == textType) {
                return text;
            }
        }
        return null;
    }

    public ImgData getImg(ImageTypes imageType) {
        for(ImgData img : imgs) {
            if( img.getIdImgType() == imageType) {
                return img;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return "Content{" +
                "cntData=" + cntData +
                ", imgs=" + imgs +
                ", txts=" + txts +
                '}';
    }
}
