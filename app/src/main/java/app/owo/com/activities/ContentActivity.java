package app.owo.com.activities;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import app.owo.com.Constants;
import app.owo.com.asyntasks.DownloadImageAsynctask;
import app.owo.com.database.Content;
import app.owo.com.database.NewsDao;
import app.owo.com.enumerations.CategoryTypes;
import app.owo.com.enumerations.ImageTypes;
import app.owo.com.enumerations.SubCategoryTypes;
import app.owo.com.enumerations.TextTypes;

public class ContentActivity extends AppCompatActivity {

    private int position;
    private Content content;
    private Toolbar toolbar;
    private TextView tvSubCategoryContent;
    private TextView tvTitularContent;
    private ImageView ivBigImageContent;
    private TextView tvEntradillaContent;
    private TextView tvEncabezadoListaContent;
    private TextView tvListaContent;
    private TextView tvCuerpoNoticiaContent;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_content);

        Intent intent = getIntent();
        position = intent.getIntExtra("position", -1);
        content = (Content) intent.getSerializableExtra("content");

        toolbar = (Toolbar) findViewById(R.id.appbarContent);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.left);

        getSupportActionBar().setTitle(CategoryTypes.getNameCateg(content.getCntData().getIdCat()));

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
//                onKeyDown(KeyEvent.KEYCODE_BACK, new KeyEvent(KeyEvent.KEYCODE_BACK, KeyEvent.KEYCODE_M));
            }
        });

        tvSubCategoryContent = (TextView) findViewById(R.id.subCategoryContent);
        tvTitularContent = (TextView) findViewById(R.id.titularContent);
        ivBigImageContent = (ImageView) findViewById(R.id.bigImageContent);
        tvEntradillaContent = (TextView) findViewById(R.id.entradillaContent);
        tvEncabezadoListaContent = (TextView) findViewById(R.id.encabezadoListaContent);
        tvListaContent = (TextView) findViewById(R.id.listaContent);
        tvCuerpoNoticiaContent = (TextView) findViewById(R.id.cuerpoNoticiaContent);
        progressBar = (ProgressBar) findViewById(R.id.progressBarContent);

        tvSubCategoryContent.setText(SubCategoryTypes.getNameCateg(content.getCntData().getIdSubCat()));
        if( content.getText(TextTypes.title) != null ) {
            tvTitularContent.setText(content.getText(TextTypes.title).getText());
        }

        new DownloadImageAsynctask(ivBigImageContent, progressBar).execute(Constants.URL_SERVER_PRO + content.getImg(ImageTypes.banner).getPath());

        if( content.getText(TextTypes.mediumparagraph) != null ) {
            tvEntradillaContent.setText(Html.fromHtml(content.getText(TextTypes.mediumparagraph).getText()));
        }else
        {
            tvEntradillaContent.setVisibility(View.GONE);
        }

        if( content.getText(TextTypes.listheader) != null ) {
            tvEncabezadoListaContent.setText(Html.fromHtml(content.getText(TextTypes.listheader).getText()));
        }else
        {
            tvEncabezadoListaContent.setVisibility(View.GONE);
        }

        if( content.getText(TextTypes.list) != null ) {
            tvListaContent.setText(Html.fromHtml(content.getText(TextTypes.list).getText()));
        }else
        {
            tvListaContent.setVisibility(View.GONE);
        }

        if( content.getText(TextTypes.longparagraph) != null )  {
            tvCuerpoNoticiaContent.setText(Html.fromHtml(content.getText(TextTypes.longparagraph).getText()));
        }else
        {
            tvCuerpoNoticiaContent.setVisibility(View.GONE);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_content, menu);

        MenuItem item = menu.findItem(R.id.action_facIcon);

        Drawable star = getResources().getDrawable( R.drawable.star);
        Drawable starSelect = getResources().getDrawable(R.drawable.star_c);

        if( content.getCntData().isFav() ) item.setIcon(starSelect);
        else    item.setIcon(star);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch ( id ) {
            case R.id.action_facIcon:
                NewsDao newsDao = new NewsDao(getApplicationContext());
                newsDao.changeFav(content.getCntData().getId());
                newsDao.closeHelper();

                Drawable star = getResources().getDrawable( R.drawable.star);
                Drawable starSelect = getResources().getDrawable( R.drawable.star_c);

                if( content.getCntData().isFav() ) {
                    content.getCntData().setFav(false);
                    item.setIcon(star);
                }
                else {
                    content.getCntData().setFav(true);
                    item.setIcon(starSelect);
                }
                break;
            case R.id.action_fav:
                Intent intent = new Intent(ContentActivity.this, FavActivity.class);
                startActivity(intent);
                break;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return super.onKeyDown(keyCode, event);
    }
}