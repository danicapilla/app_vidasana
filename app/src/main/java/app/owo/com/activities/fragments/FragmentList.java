package app.owo.com.activities.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.io.Serializable;
import java.util.List;

import app.owo.com.Constants;
import app.owo.com.activities.R;
import app.owo.com.asyntasks.DownloadImageAsynctask;
import app.owo.com.asyntasks.NewsAsynctask;
import app.owo.com.asyntasks.OnTaskCompleted;
import app.owo.com.database.Content;
import app.owo.com.database.NewsDao;
import app.owo.com.enumerations.CategoryTypes;
import app.owo.com.enumerations.ImageTypes;
import app.owo.com.enumerations.SubCategoryTypes;
import app.owo.com.enumerations.TextTypes;
import app.owo.com.activities.ContentActivity;

/**
 * Created by vsantoja on 8/06/15.
 */
public class FragmentList extends Fragment implements OnTaskCompleted
{
    private static List<Content> contents;
    private int position;
    private boolean loading = true;
    private int firstVisibleItem, visibleItemCount, totalItemCount;
    private int current_page;
    private AdapterContentList adapterContentList;
    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    private int idCat;

    public FragmentList() {}

    public static FragmentList newInstance(List<Content> contentList, int position, int idCat) {
        contents = contentList;
        FragmentList fragment = new FragmentList();

        Bundle args = new Bundle();
        args.putSerializable("contents", (Serializable) contents);
        args.putInt("position", position);
        args.putInt("idCat", idCat);

        fragment.setArguments(args);
        return fragment;
    }

    // Store instance variables based on arguments passed
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        contents = (List<Content>) getArguments().getSerializable("contents");
        position = getArguments().getInt("position");
        idCat = getArguments().getInt("idCat");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.reciclerview_layout, container, false);

        progressBar = (ProgressBar) view.findViewById(R.id.progressBarRecyclerView);
        recyclerView = (RecyclerView) view.findViewById(R.id.RecViewList);
        adapterContentList = new AdapterContentList(contents);

        if (contents.size() != 0) {
            progressBar.setVisibility(View.GONE);
            recyclerView.setAdapter(adapterContentList);
            final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this.getActivity(), LinearLayoutManager.VERTICAL, false);
            recyclerView.setLayoutManager(linearLayoutManager);

            recyclerView.addOnScrollListener(new EndlessRecyclerOnScrollListener(linearLayoutManager));
        } else {
            NewsAsynctask asynctask = new NewsAsynctask(getActivity(), this);
            String[] params = new String[2];
            params[0] = String.valueOf(idCat);
            params[1] = "0";
            asynctask.execute(params);
        }

        return view;
    }

    @Override
    public void onTaskCompleted() {
        NewsDao newsDao = new NewsDao(getActivity());

        contents.clear();
        contents = newsDao.getCnt(CategoryTypes.get(idCat)) ;

        newsDao.closeHelper();

        adapterContentList = new AdapterContentList(contents);
        recyclerView.setAdapter(adapterContentList);
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this.getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);

        recyclerView.addOnScrollListener(new EndlessRecyclerOnScrollListener(linearLayoutManager));
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onTaskIncompleted() {

    }

    public class AdapterContentList extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        List<Content> contents;

        public AdapterContentList (List<Content> contents) {
            this.contents = contents;
        }

        class ContentSmallViewHolder extends RecyclerView.ViewHolder {
            TextView textViewTitulo;
            TextView textViewSubtitulo;
            TextView textViewSubcategoria;
            ImageView imageView;
            ProgressBar progressBar;

            public ContentSmallViewHolder(View itemView) {
                super(itemView);

                textViewTitulo = (TextView) itemView.findViewById(R.id.titularItemTextViewSmall);
                textViewSubtitulo = (TextView) itemView.findViewById(R.id.parrafoItemTextViewSmall);
                textViewSubcategoria = (TextView) itemView.findViewById(R.id.subCategoryTextViewSmall);
                imageView = (ImageView) itemView.findViewById(R.id.smallImageItem);
                progressBar = (ProgressBar) itemView.findViewById(R.id.progressBarSmall);

            }

            public void insertContent( Content content ) {
                if( content.getText(TextTypes.title) != null ) {
                    textViewTitulo.setText(content.getText(TextTypes.title).getText());
                }

                if(content.getText(TextTypes.shortparagraph) != null ) {
                    textViewSubtitulo.setText(content.getText(TextTypes.shortparagraph).getText());
                }

                textViewSubcategoria.setText(SubCategoryTypes.getNameCateg(content.getCntData().getIdSubCat()));

                new DownloadImageAsynctask(imageView, progressBar).execute(Constants.URL_SERVER_PRO + content.getImg(ImageTypes.small).getPath());
            }
        }

        class ContentBigViewHolder extends RecyclerView.ViewHolder {

            TextView textViewTitulo;
            TextView textViewSubtitulo;
            TextView textViewSubcategoria;
            ImageView imageView;
            ProgressBar progressBar;

            public ContentBigViewHolder(View itemView) {
                super(itemView);

                textViewTitulo = (TextView) itemView.findViewById(R.id.titularItemTextViewBig);
                textViewSubtitulo = (TextView) itemView.findViewById(R.id.parrafoItemTextViewBig);
                textViewSubcategoria = (TextView) itemView.findViewById(R.id.subCategoryTextViewBig);
                imageView = (ImageView) itemView.findViewById(R.id.bigImageItem);
                progressBar = (ProgressBar) itemView.findViewById(R.id.progressBarBig);
            }

            public void insertContent( Content content ) {
                if( content.getText(TextTypes.title) != null ) {
                    textViewTitulo.setText(content.getText(TextTypes.title).getText());
                }

                if(content.getText(TextTypes.shortparagraph) != null ) {
                    textViewSubtitulo.setText(content.getText(TextTypes.shortparagraph).getText());
                }

                textViewSubcategoria.setText(SubCategoryTypes.getNameCateg(content.getCntData().getIdSubCat()));

                new DownloadImageAsynctask(imageView, progressBar).execute(Constants.URL_SERVER_PRO +content.getImg(ImageTypes.banner).getPath());
            }
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int type)
        {
            View itemView = null;
            RecyclerView.ViewHolder viewHolder = null;
            switch (type){
                case 0:
                    itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.bigcontent_item, viewGroup, false);
                    viewHolder = new ContentBigViewHolder(itemView);
                    break;
                case 1:
                    itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.smallcontent_item, viewGroup, false);
                    viewHolder = new ContentSmallViewHolder(itemView);
                    break;
            }

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = recyclerView.getChildAdapterPosition(v);
                    Intent intent = new Intent( getActivity(), ContentActivity.class);
                    intent.putExtra("position", position);
                    intent.putExtra("content", contents.get(position));
                    startActivity(intent);
                }
            });

            return viewHolder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position)
        {
            Content content = contents.get(position);

            if( viewHolder instanceof ContentBigViewHolder ) ((ContentBigViewHolder) viewHolder).insertContent(content);
                else if ( viewHolder instanceof ContentSmallViewHolder ) ((ContentSmallViewHolder) viewHolder).insertContent(content);
        }

        @Override
        public int getItemCount()
        {
            return contents.size();
        }

        @Override
        public int getItemViewType(int position) {
            if (position == 0 ) {
                return 0;
            } else {
                return 1;
            }
        }


    }

    public class EndlessRecyclerOnScrollListener extends RecyclerView.OnScrollListener implements OnTaskCompleted
    {
        private int previousTotal = 0;
        private LinearLayoutManager linearLayoutManager;

        public EndlessRecyclerOnScrollListener (LinearLayoutManager linearLayoutManager) {
            this.linearLayoutManager = linearLayoutManager;
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

            visibleItemCount = recyclerView.getChildCount();
            totalItemCount = linearLayoutManager.getItemCount();
            firstVisibleItem = linearLayoutManager.findFirstVisibleItemPosition();

            if (loading) {
                if (totalItemCount > previousTotal) {
                    loading = false;
                    previousTotal = totalItemCount;
                }

                if (!loading && (visibleItemCount + firstVisibleItem) >= totalItemCount ) {
                    loading = true;

                    current_page++;

                    Log.i(FragmentList.class.getName(), "Estamos abajo");

//                    NewsAsynctask newsAsynctask = new NewsAsynctask(getActivity(), this);
//                    String[] params = new String[2];
//                    params[0] = String.valueOf(idCat);
//                    params[1] = String.valueOf( totalItemCount );
//                    newsAsynctask.execute(params);
                }
            }
        }


        @Override
        public void onTaskCompleted() {
            NewsDao newsDao = new NewsDao(getActivity());

            contents.clear();
            contents = newsDao.getCnt(CategoryTypes.get(idCat)) ;

            newsDao.closeHelper();

            adapterContentList.notifyDataSetChanged();

        }

        @Override
        public void onTaskIncompleted() {

        }
    }
}