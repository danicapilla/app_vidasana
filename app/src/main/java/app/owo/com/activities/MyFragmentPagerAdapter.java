package app.owo.com.activities;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import app.owo.com.Constants;
import app.owo.com.database.Content;
import app.owo.com.database.NewsDao;
import app.owo.com.enumerations.CategoryTypes;
import app.owo.com.activities.fragments.FragmentList;

/**
 * Created by vsantoja on 8/06/15.
 */
public class MyFragmentPagerAdapter extends FragmentStatePagerAdapter {

    private String[] tabtitles = new String[Constants.PAGE_COUNT];
    private List<String> cat;
    private List<Content> contents;
    private Context context;
    private int position;

    public MyFragmentPagerAdapter(FragmentManager supportFragmentManager, Context context)
    {
        super(supportFragmentManager);
        this.context = context;

        cat = new ArrayList<>();
        for (CategoryTypes categoryTypes : CategoryTypes.values()) {
            cat.add(CategoryTypes.getNameCateg(categoryTypes));
        }
        tabtitles = cat.toArray(new String[0]);
    }

    @Override
    public Fragment getItem(int position) {

        Fragment f = null;

        Log.i(MyFragmentPagerAdapter.class.getName(), "Position: " + position);
        contents = new ArrayList<>();

        NewsDao newsDao = new NewsDao(context);
        int idCat = 0;
//        NewsAsynctask asynctask = new NewsAsynctask(context, this);
//        String[] params = new String[2];
//        params[1] = "0";

        switch(position) {
            case 0:
                contents = newsDao.getCnt(CategoryTypes.portada);
                idCat = CategoryTypes.portada.getCode();
                break;
            case 1:

//                if( !newsDao.existContent(CategoryTypes.serMadre) ) {
//                    params[0] = String.valueOf(CategoryTypes.serMadre.getCode());
//                    asynctask.execute(params);
//                }

                contents = newsDao.getCnt(CategoryTypes.recetas);
                idCat = CategoryTypes.recetas.getCode();
                break;
            case 2:

//                if( !newsDao.existContent(CategoryTypes.psicologiaPareja) ) {
//                    params[0] = String.valueOf(CategoryTypes.psicologiaPareja.getCode());
//                    asynctask.execute(params);
//                }

                contents = newsDao.getCnt(CategoryTypes.deporte);
                idCat = CategoryTypes.deporte.getCode();
                break;
            case 3:

//                if( !newsDao.existContent(CategoryTypes.hogarDeco) ) {
//                    params[0] = String.valueOf(CategoryTypes.hogarDeco.getCode());
//                    asynctask.execute(params);
//                }

                contents = newsDao.getCnt(CategoryTypes.nutricion);
                idCat = CategoryTypes.nutricion.getCode();
                break;
            case 4:

//                if( !newsDao.existContent(CategoryTypes.musicaNosotras) ) {
//                    params[0] = String.valueOf(CategoryTypes.musicaNosotras.getCode());
//                    asynctask.execute(params);
//                }

                contents = newsDao.getCnt(CategoryTypes.salud);
                idCat = CategoryTypes.salud.getCode();
                break;
            case 5:

//                if( !newsDao.existContent(CategoryTypes.novias) ) {
//                    params[0] = String.valueOf(CategoryTypes.novias.getCode());
//                    asynctask.execute(params);
//                }

                contents = newsDao.getCnt(CategoryTypes.ocioGourmet);
                idCat = CategoryTypes.ocioGourmet.getCode();
                break;
            case 6:

//                if( !newsDao.existContent(CategoryTypes.shoppingTecnologia) ) {
//                    params[0] = String.valueOf(CategoryTypes.shoppingTecnologia.getCode());
//                    asynctask.execute(params);
//                }

                contents = newsDao.getCnt(CategoryTypes.sexualidadSaludable);
                idCat = CategoryTypes.sexualidadSaludable.getCode();
                break;
        }

        f = FragmentList.newInstance(contents, position, idCat);

        newsDao.closeHelper();

        return f;
    }

    @Override
    public int getCount() {
        return Constants.PAGE_COUNT;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabtitles[position];
    }
}
