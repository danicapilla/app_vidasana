package app.owo.com.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ProgressBar;
import android.widget.TextView;

import app.owo.com.asyntasks.NewsAsynctask;
import app.owo.com.asyntasks.OnTaskCompleted;
import app.owo.com.enumerations.CategoryTypes;

public class LoadActivity extends Activity implements OnTaskCompleted
{
    private ProgressBar progressBarLoad;
    private TextView textViewLoad;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_load);

        textViewLoad = (TextView) findViewById(R.id.textViewLoad);
        textViewLoad.setText(R.string.text1_load);

        NewsAsynctask newsAsynctask = new NewsAsynctask(this, this);
        String[] params = new String[2];

        params[0] = String.valueOf(CategoryTypes.portada.getCode());
        params[1] = "0";

        newsAsynctask.execute(params);
    }

    @Override
    public void onTaskCompleted()
    {
        Log.i(LoadActivity.class.getName(), "Se ha completado con éxito.");
        Intent i = new Intent(LoadActivity.this, FragActivity.class);
        startActivity(i);
        overridePendingTransition(R.animator.left_in, R.animator.left_out);
        finish();
    }

    @Override
    public void onTaskIncompleted()
    {

    }
}