package app.owo.com.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;

import app.owo.com.Constants;
import app.owo.com.asyntasks.DownloadImageAsynctask;
import app.owo.com.database.Content;
import app.owo.com.database.NewsDao;
import app.owo.com.enumerations.ImageTypes;
import app.owo.com.enumerations.SubCategoryTypes;
import app.owo.com.enumerations.TextTypes;

public class FavActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private RecyclerView recyclerView;
    private AdapterContentList adapterContentList;
    private List<Content> contents;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fav);

        toolbar = (Toolbar) findViewById(R.id.appbarFav);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.left);
        getSupportActionBar().setTitle("Favoritos");

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
//                onKeyDown(KeyEvent.KEYCODE_BACK, new KeyEvent(KeyEvent.KEYCODE_BACK, KeyEvent.KEYCODE_M));
            }
        });

        NewsDao newsDao = new NewsDao(this);
        contents = newsDao.getCntFav();
        newsDao.closeHelper();

        adapterContentList = new AdapterContentList(contents);

        recyclerView = (RecyclerView) findViewById(R.id.RecViewListFav);
        recyclerView.setAdapter(adapterContentList);
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_fav, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public class AdapterContentList extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        List<Content> contents;

        public AdapterContentList (List<Content> contents) {
            this.contents = contents;
        }

        class ContentViewHolder extends RecyclerView.ViewHolder {
            TextView textViewTitulo;
            TextView textViewSubtitulo;
            TextView textViewSubcategoria;
            ImageView imageView;
            ProgressBar progressBar;

            public ContentViewHolder(View itemView) {
                super(itemView);

                textViewTitulo = (TextView) itemView.findViewById(R.id.titularItemTextViewSmall);
                textViewSubtitulo = (TextView) itemView.findViewById(R.id.parrafoItemTextViewSmall);
                textViewSubcategoria = (TextView) itemView.findViewById(R.id.subCategoryTextViewSmall);
                imageView = (ImageView) itemView.findViewById(R.id.smallImageItem);
                progressBar = (ProgressBar) itemView.findViewById(R.id.progressBarSmall);

            }

            public void insertContent( Content content ) {
                if( content.getText(TextTypes.title) != null ) {
                    textViewTitulo.setText(content.getText(TextTypes.title).getText());
                }

                if(content.getText(TextTypes.shortparagraph) != null ) {
                    textViewSubtitulo.setText(content.getText(TextTypes.shortparagraph).getText());
                }

                textViewSubcategoria.setText(SubCategoryTypes.getNameCateg(content.getCntData().getIdSubCat()));

                new DownloadImageAsynctask(imageView, progressBar).execute(Constants.URL_SERVER_PRO + content.getImg(ImageTypes.small).getPath());
            }
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int type)
        {
            View itemView = null;
            RecyclerView.ViewHolder viewHolder = null;
            switch (type){
                case 1:
                    itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.smallcontent_item, viewGroup, false);
                    viewHolder = new ContentViewHolder(itemView);
                    break;
            }

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = recyclerView.getChildAdapterPosition(v);
                    Intent intent = new Intent( getApplicationContext(), ContentActivity.class);
                    intent.putExtra("position", position);
                    intent.putExtra("content", contents.get(position));
                    startActivity(intent);
                }
            });

            return viewHolder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position)
        {
            Content content = contents.get(position);
            ((ContentViewHolder) viewHolder).insertContent(content);
        }

        @Override
        public int getItemCount()
        {
            return contents.size();
        }

        @Override
        public int getItemViewType(int position) {
            return 1;
        }


    }
}
