package app.owo.com.asyntasks;

/**
 * Created by vsantoja on 8/06/15.
 */
public interface OnTaskCompleted {
    void onTaskCompleted();
    void onTaskIncompleted();
}
