package app.owo.com.asyntasks;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

import app.owo.com.Constants;
import app.owo.com.database.NewsDao;
import app.owo.com.enumerations.CategoryTypes;

/**
 * Created by vsantoja on 27/05/15.
 *
 * En Android no se permiten realizar conexiones a internet en primer plano, por eso tenemos que utilizar
 *      o hilos o tareas asincronas (estas últimas mas eficientes ya que consumen menos recursos).
 *
 * Toda AsyncTask por defecto debe implementar el método doInBackground (basicamente lo que queremos que haga la tarea)
 *      pero es cierto que existen otros métodos que pueden resultarnos útiles: onPreExecuted() que se ejecuta antes de
 *      la ejecución de doInBackground(), doInBackground() que contiene el código principal de la tarea, onProgressUpdate()
 *      un método que se ejecuta si queremos actualizar el progreso de una tarea (llamando desde doInBackground al método publishProgress()),
 *      onPostExecuted() que es lo que se ejecuta justo después de doInBAckground() y por último onCancelled() que se ejecuta si se cancela
 *      la ejecución antes de acabar la tarea.
 *
 * Las Asynctask siempre tienen 3 valores (extends AsyncTask <tipoValor1, tipoValor2, tipoValor3>)
 *      el primero de ellos es el tipo de valor de entrada (el valor que envíamos a la hora de ejecutar la acción)
 *      el segundo es el tipo de valor que se va a enviar para actualizar el progreso de la tarea
 *      y el tercero es el tipo de valor del resultado que va a recibir onPostExecuted()
 *
 */

public class NewsAsynctask extends AsyncTask<String, Void, String>
{
    private Context context;
    private OnTaskCompleted listener;
    private int categoryId;
    private static HashMap<Integer, Enum> codeValueMap = new HashMap<>();

    public NewsAsynctask(Context context, OnTaskCompleted listener) {
        this.listener = listener;
        this.context = context;
    }

    public NewsAsynctask(Context context) {
        this.context = context;
        this.listener = null;
    }

    @Override
    protected String doInBackground(String... params)
    {
        //idSite -> el id de la aplicación
        //idCat -> el id de la categoría
        //first -> cual es el primero en devolver la primera vez 0 las siguientes de 50 en 50

        codeValueMap.clear();
        for( CategoryTypes categoryTypes : CategoryTypes.values() ) {
            codeValueMap.put(categoryTypes.getCode(), categoryTypes);
        }

        CategoryTypes category = (CategoryTypes) codeValueMap.get( Integer.parseInt( params[0] ) );
        categoryId = category.getCode();
        String first = params[1];

        try
        {
            StringBuilder urlBuilder = new StringBuilder();
            urlBuilder.append(Constants.URL);
            urlBuilder.append(Constants.NEWS_PETITION);
            urlBuilder.append("?");
            urlBuilder.append("idsite=" + Constants.ID_SITE);
            urlBuilder.append("&");
            urlBuilder.append("idcat=" + category.getCode());
            urlBuilder.append("&");
            urlBuilder.append("first=" + first);

            Log.i(AsyncTask.class.getName(), "Url: " + urlBuilder.toString());

            URL url = new URL(urlBuilder.toString());

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            connection.setRequestMethod(Constants.GET);

            int responseCode = connection.getResponseCode();

            Log.i(AsyncTask.class.getName(), "Response Code: " + responseCode);

            if( responseCode == 200 ) {
                BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();

                Log.i(AsyncTask.class.getName(), "Response: " + response);

                return response.toString();
            } else {
                return null;
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    protected void onPostExecute(String response) {
        super.onPostExecute(response);

        if( response != null ) {
            try {
                JSONObject objectResponse = new JSONObject(response);

                NewsDao newsDao = new NewsDao(context);
                newsDao.saveNews(objectResponse.getJSONArray(Constants.ARRAY_NEWS));
                newsDao.closeHelper();
                Log.i(NewsAsynctask.class.getName(), "Guardado");
                if( listener != null ) {
                    listener.onTaskCompleted();
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {

        }
    }
}