package app.owo.com.enumerations;

/**
 * Created by vsantoja on 28/05/15.
 */

public enum ImageTypes
{
    /*1 banner*/
    banner(1),

    /*2 medium*/
    medium(2),

    /*3 icon*/
    icon(3),

    /*4 image*/
    image(4),

    /*5 class*/
    forclass(5),

    /*6 small*/
    small(6),

    /*7 gif*/
    gif(7);

    private int code;

    ImageTypes(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
