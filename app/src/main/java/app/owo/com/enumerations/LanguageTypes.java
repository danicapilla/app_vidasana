package app.owo.com.enumerations;

/**
 * Created by vsantoja on 28/05/15.
 */
public enum LanguageTypes
{
    /*1 español */
    espannol(1);

    private int code;

    LanguageTypes(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
