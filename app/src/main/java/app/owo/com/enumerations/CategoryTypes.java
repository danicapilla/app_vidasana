package app.owo.com.enumerations;

/**
 * Created by vsantoja on 28/05/15.
 */
public enum CategoryTypes
{
    portada(0),
    recetas(90),
    deporte(29),
    nutricion(28),
    salud(30),
    ocioGourmet(32),
    sexualidadSaludable(1357);

    private int code;

    CategoryTypes(int code) {
        this.code = code;
    }


    public int getCode() {
        return code;
    }

    public static CategoryTypes get(int code) {
        for(CategoryTypes s : values()) {
            if(s.code == code) return s;
        }
        return null;
    }

    public static String getNameCateg(CategoryTypes cat) {
        String name;
        switch (cat) {
            case portada:
                name = "Portada";
                break;
            case recetas:
                name = "Recetas";
                break;
            case deporte:
                name = "deporte";
                break;
            case nutricion:
                name = "nutricion";
                break;
            case salud:
                name = "Salud";
                break;
            case ocioGourmet:
                name = "ocioGourmet";
                break;
            case sexualidadSaludable:
                name = "Sexualidad Saludable";
                break;

            default:
                name = "";
        }
        return name;
    }

}
