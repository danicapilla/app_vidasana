package app.owo.com.enumerations;

/**
 * Created by vsantoja on 28/05/15.
 */
public enum TextTypes
{
    /*1 name*/
    name(1),

    /*2 title*/
    title(2),

    /*3 subtitle*/
    subtitle(3),

    /*4 shortparagraph parrafo corto*/
    shortparagraph(4),

    /*5 mediumparagraph*/
    mediumparagraph(5),

    /*6 longparagraph*/
    longparagraph(6),

    /*7 list header*/
    listheader(7),

    /*7 list*/
    list(8),

    /*12 aux1*/
    aux1 (12),

    /*13 aux2*/
    aux2 (13),

    /*14 aux3*/
    aux3 (14),

    /*16 altimage*/
    altimage (16);


    private int code;

    TextTypes(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
