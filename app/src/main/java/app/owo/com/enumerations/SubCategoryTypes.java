package app.owo.com.enumerations;

/**
 * Created by vsantoja on 11/06/15.
 */
public enum SubCategoryTypes {

    /*SUBCATEGORIAS*/
    portada(0),
    cocinaSana(33),
    cocinaSingles(34),
    cocinaSinGluten(35),
    cocinamama(36),
    reposteria(37),
    dietas(38),
    educacionAlimenticia(39),
    entrenamiento(43),
    gimnasio(44),
    runners(45),
    accesorios(46),
    lesiones(47),
    ocioSaludable(52),
    gourmet(53),
    buenosHabitos(96),
    doctorCasa(97),
    sexologia(1358),
    sexoSalud(1359);

    private int code;

    SubCategoryTypes(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public static SubCategoryTypes get(int code) {
        for(SubCategoryTypes s : values()) {
            if(s.code == code) return s;
        }
        return null;
    }

    public static String getNameCateg(SubCategoryTypes cat) {
        String name;
        switch (cat) {
            case portada:
                name = "Portada";
                break;
            case cocinaSana:
                name = "Cocina Sana";
                break;
            case cocinaSingles:
                name = "Cocina para singles";
                break;
            case cocinaSinGluten:
                name = "Cocina sin gluten";
                break;
            case cocinamama:
                name = "cocina de mama";
                break;
            case reposteria:
                name = "Reposteria";
                break;
            case dietas:
                name = "Dietas";
                break;
            case educacionAlimenticia:
                name = "Educacion alimentaria";
                break;
            case entrenamiento:
                name = "Entrenamiento";
                break;
            case gimnasio:
                name = "Gimnasio";
                break;
            case runners:
                name = "Dietas";
                break;
            case accesorios:
                name = "accesorios";
                break;
            case lesiones:
                name = "Entrenamiento";
                break;
            case ocioSaludable:
                name = "Ocio saludable";
                break;
            case gourmet:
                name = "Gourmet";
                break;
            case buenosHabitos:
                name = "Buenos hábitos";
                break;
            case doctorCasa:
                name = "Doctor en casa";
                break;
            case sexologia:
                name = "Sexologia";
                break;
            case sexoSalud:
                name = "Sexo y salud";
                break;


            default:
                name = "";
        }
        return name;
    }
}
