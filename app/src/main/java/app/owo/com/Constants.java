package app.owo.com;

/**
 * Created by vsantoja on 27/05/15.
 */
public class Constants
{
    //public static final String URL = "http://10.0.3.2:8080/service_apps/";
    public static final String URL = "http://146.255.103.188:18080/service_apps/";

    public static final String URL_SERVER_PRO = "http://files.onwhyon.com/3/resources/";

    public static final String NEWS_PETITION = "api/news";

    public static final String GET = "GET";
    public static final String POST = "POST";

    public static final int ID_SITE = 3;

    public static final String DATABASE_NAME = "owoAndroid.db";
    public static final int DATABASE_VERSION = 1;

    public static final String ARRAY_NEWS = "cntResponse";

    public static final int PAGE_COUNT = 7;
    public static final long MAX_CONTS = 20;
}
